"use strict";
console.log(123)
var renderer,labelRenderer,scene,camera,light,box,edge,line,controls,canvas;//base
var plane1,plane2,plane3,plane4,plane5,plane6;//face
var line_top1,line_top2,line_top3,line_top4,
    line_middle1,line_middle2,line_middle3,line_middle4,
    line_bottom1,line_bottom2,line_bottom3,line_bottom4;//line
var bline_top1,bline_top2,bline_top3,bline_top4,
    bline_middle1,bline_middle2,bline_middle3,bline_middle4,
    bline_bottom1,bline_bottom2,bline_bottom3,bline_bottom4;//bline
var textGeometry,textMaterial,text_A,text_B,text_C,text_D,text_E,text_F,text_G,text_H;//Alphabets
var p_geometry,p_geometry2,p_material,l_hol_geometry,l_ver_geometry,l_material;//each settings

var width = 1024;
var height = 768;

var num=0;var a=[];var b=[];var d=[];
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var offset = new THREE.Vector3();
var intersection = new THREE.Vector3();
var selectObj;
var body = $('body');

const meshList = [];
const meshList2 = [];
const list = [];
var fun=0;

var rotateBox = false;
var rotateStart = {x: 0, y: 0};
var pointerId;
var mouseDown, mouseMove, mouseUp;
var pick = {isPick: false, x: null, y: null}; //Detect user click to pick an object or move mouse.
var bodyWidth = Math.max(window.innerWidth, 1024), bodyHeight = Math.max(window.innerHeight, height);

window.onresize = function () {
    bodyWidth = Math.max(window.innerWidth, 1024);
    bodyHeight = Math.max(window.innerHeight, 768);
    body.width(bodyWidth);
    body.height(bodyHeight);
    $('canvas').css("width", 1024).css("height", 768);

    //Zoom camera
    camera.zoom = detectZoom.zoom();
    camera.aspect = bodyWidth / bodyHeight;
    camera.updateProjectionMatrix();
};

window.onload = function () {
    //Setting name for event
    if (isTouchDevice()) {
        if (getBrowserName() === "Safari") {
            mouseDown = 'touchstart';
            mouseMove = 'touchmove';
            mouseUp = 'touchend';
        } else {
            mouseDown = 'pointerdown';
            mouseMove = 'pointermove';
            mouseUp = 'pointerup';
        }
    }
     else {
        mouseDown = 'mousedown';
        mouseMove = 'mousemove';
        mouseUp = 'mouseup';
    }

    //Add event setting pick or not
    document.addEventListener(mouseDown, function (e) {
        pick.isPick = true;
        pick.x = e.pageX;
        pick.y = e.pageY;
    });
    document.addEventListener(mouseMove, function (e) {
        if (pick.x - 5 >= e.pageX || pick.x + 5 <= e.pageX || pick.y - 5 >= e.pageY || pick.y + 5 <= e.pageY)
            pick.isPick = false;
    });

    init();
    render();
};

function webglAvailable() {
    try {
        return !!(window.WebGLRenderingContext && (
                testCanvas.getContext('webgl') ||
                testCanvas.getContext('experimental-webgl'))
        );
    } catch (e) {
        return false;
    }
}

function init() {
    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    renderer.setSize(width, height, true);
    renderer.setPixelRatio(window.devicePixelRatio);
    $('#three_view').append(renderer.domElement);

    labelRenderer = new THREE.CSS2DRenderer();
    labelRenderer.setSize(width, height, true);
    labelRenderer.domElement.style.position = 'absolute';
    labelRenderer.domElement.id = 'text';
    labelRenderer.domElement.style.top = '0px';
    $('#three_view').append( labelRenderer.domElement );

    scene = new THREE.Scene();
    scene.rotation.x=0.4;
    scene.rotation.y=-0.4;

    camera = new THREE.OrthographicCamera(width / - 2, width / 2, height / 2, height / - 2, 0.1, 1000);
    camera.translateZ(400);
    camera.zoom = detectZoom.zoom();
    camera.updateProjectionMatrix();

    light = new THREE.DirectionalLight( 0xffffff );
    scene.add( light );

    createLines();
    var resolution = new THREE.Vector2( width, height );

    //Edge line of the box
    function makeLine( geo ) {

    var g2 = new MeshLine();
    g2.setGeometry( geo );

    var material = new MeshLineMaterial( {
        useMap: false,
        color: new THREE.Color( 0x000000 ),
        opacity: 1,
        resolution: resolution,
        sizeAttenuation: true,
        lineWidth: .003,
        near: camera.near,
        far: camera.far
    });
    var mesh = new THREE.Mesh( g2.geometry, material );
    scene.add( mesh );

    }

    function createLines() {
    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, 50, -100 ) );
    line.vertices.push( new THREE.Vector3( 100, 50, -100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, 50, -100 ) );
    line.vertices.push( new THREE.Vector3( -100, 50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, 50, 100 ) );
    line.vertices.push( new THREE.Vector3( 100, 50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( 100, 50, 100 ) );
    line.vertices.push( new THREE.Vector3( 100, 50, -100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, 50, -100 ) );
    line.vertices.push( new THREE.Vector3( -100, -50, -100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, 50, 100 ) );
    line.vertices.push( new THREE.Vector3( -100, -50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( 100, 50, 100 ) );
    line.vertices.push( new THREE.Vector3( 100, -50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( 100, 50, -100 ) );
    line.vertices.push( new THREE.Vector3( 100, -50,-100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, -50, -100 ) );
    line.vertices.push( new THREE.Vector3( 100, -50, -100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, -50, -100 ) );
    line.vertices.push( new THREE.Vector3( -100, -50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( -100, -50, 100 ) );
    line.vertices.push( new THREE.Vector3( 100, -50, 100 ) );
    makeLine( line );

    var line = new THREE.Geometry();
    line.vertices.push( new THREE.Vector3( 100, -50, 100 ) );
    line.vertices.push( new THREE.Vector3( 100, -50, -100 ) );
    makeLine( line );
    }

    //Alphabets
    textGeometry = new THREE.PlaneBufferGeometry(20,30);
    textMaterial = new THREE.MeshBasicMaterial( {color: 0x00ffff, side: THREE.FrontSide,transparent:true,opacity: 0});
    text_A = new THREE.Mesh( textGeometry, textMaterial );
    text_B = text_A.clone();text_C = text_A.clone();text_D = text_A.clone();text_E = text_A.clone();text_F = text_A.clone();text_G = text_A.clone();text_H = text_A.clone();

    text_A.renderOrder=text_B.renderOrder=text_C.renderOrder=text_D.renderOrder=text_E.renderOrder=text_F.renderOrder=text_G.renderOrder=text_H.renderOrder=200;

    text_A.position.set(-110,70,-110);
    text_B.position.set(-110,70,110);
    text_C.position.set(110,70,110);
    text_D.position.set(110,70,-110);
    text_E.position.set(-110,-70,-110);
    text_F.position.set(-110,-70,110);
    text_G.position.set(110,-70,110);
    text_H.position.set(110,-70,-110);

    var a = document.createElement( 'div' );
    a.className = 'label';
    a.id = 'A';
    a.textContent = 'A';
    a.style.fontSize = '30px';
    var aLabel = new THREE.CSS2DObject( a );
    text_A.add( aLabel );

    var b = document.createElement( 'div' );
    b.className = 'label';
    b.textContent = 'B';
    b.style.fontSize = '30px';
    var bLabel = new THREE.CSS2DObject( b );
    text_B.add( bLabel );

    var c = document.createElement( 'div' );
    c.className = 'label';
    c.textContent = 'C';
    c.style.fontSize = '30px';
    var cLabel = new THREE.CSS2DObject( c );
    text_C.add( cLabel );

    var d = document.createElement( 'div' );
    d.className = 'label';
    d.textContent = 'D';
    d.style.fontSize = '30px';
    var dLabel = new THREE.CSS2DObject( d );
    text_D.add( dLabel );

    var e = document.createElement( 'div' );
    e.className = 'label';
    e.textContent = 'E';
    e.style.fontSize = '30px';
    var eLabel = new THREE.CSS2DObject( e );
    text_E.add( eLabel );

    var f = document.createElement( 'div' );
    f.className = 'label';
    f.textContent = 'F';
    f.style.fontSize = '30px';
    var fLabel = new THREE.CSS2DObject( f );
    text_F.add( fLabel );

    var g = document.createElement( 'div' );
    g.className = 'label';
    g.textContent = 'G';
    g.style.fontSize = '30px';
    var gLabel = new THREE.CSS2DObject( g );
    text_G.add( gLabel );

    var h = document.createElement( 'div' );
    h.className = 'label';
    h.textContent = 'H';
    h.style.fontSize = '30px';
    var hLabel = new THREE.CSS2DObject( h );
    text_H.add( hLabel );

    //box's face settings and hit point for the edges
    p_geometry=new THREE.PlaneBufferGeometry( 200, 100);
    p_geometry2=new THREE.PlaneBufferGeometry( 200, 200);
    p_material = new THREE.MeshBasicMaterial( {color: 0xffffff, side: THREE.DoubleSide,transparent:true,opacity: 0} );

    l_hol_geometry=new THREE.CylinderBufferGeometry( 5, 5, 205, 100 );
    l_ver_geometry=new THREE.CylinderBufferGeometry( 5, 5, 105, 100 );
    l_material=new THREE.MeshBasicMaterial({color: 0x000000, side: THREE.FrontSide,transparent:true,opacity: 0});

    plane1=new THREE.Mesh(p_geometry,p_material);
    plane1.position.set( 0, 0, 100 );
    plane1.renderOrder=100;
    plane1.name="1";

    plane2=new THREE.Mesh(p_geometry,p_material);
    plane2.position.set( 100, 0, 0 );
    plane2.rotation.set(0,1/2*Math.PI,0);
    plane2.renderOrder=100;
    plane2.name="2";

    plane3=new THREE.Mesh(p_geometry,p_material);
    plane3.position.set( -100, 0, 0 );
    plane3.rotation.set(0,1/2*Math.PI,0);
    plane3.renderOrder=100;
    plane3.name="3";

    plane4=new THREE.Mesh(p_geometry,p_material);
    plane4.position.set( 0, 0, -100 );
    plane4.renderOrder=100;
    plane4.name="4";

    plane5=new THREE.Mesh(p_geometry2,p_material);
    plane5.position.set( 0, -50, 0 );
    plane5.rotation.set(1/2*Math.PI,0,0);
    plane5.renderOrder=100;
    plane5.name="5";

    plane6=new THREE.Mesh(p_geometry2,p_material);
    plane6.position.set( 0, 50, 0 );
    plane6.rotation.set(1/2*Math.PI,0,0);
    plane6.renderOrder=100;
    plane6.name="6";

    line_top1=new THREE.Mesh(l_hol_geometry,l_material);
    line_top1.renderOrder=100;
    line_top1.position.set( -100, 50, 0 );
    line_top1.rotation.set(1/2*Math.PI,0,0);
    line_top1.name="line_top1";

    line_top2=new THREE.Mesh(l_hol_geometry,l_material);
    line_top2.renderOrder=100;
    line_top2.position.set( 100, 50, 0 );
    line_top2.rotation.set(1/2*Math.PI,0,0);
    line_top2.name="line_top2";

    line_top3=new THREE.Mesh(l_hol_geometry,l_material);
    line_top3.renderOrder=100;
    line_top3.position.set( 0, 50, 100);
    line_top3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    line_top3.name="line_top3";

    line_top4=new THREE.Mesh(l_hol_geometry,l_material);
    line_top4.renderOrder=100;
    line_top4.position.set( 0, 50, -100 );
    line_top4.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    line_top4.name="line_top4";

    line_middle1=new THREE.Mesh(l_ver_geometry,l_material);
    line_middle1.renderOrder=100;
    line_middle1.position.set( -100, 0, 100 );
    line_middle1.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    line_middle1.name="line_middle1";

    line_middle2=new THREE.Mesh(l_ver_geometry,l_material);
    line_middle2.renderOrder=100;
    line_middle2.position.set( 100, 0, -100 );
    line_middle2.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    line_middle2.name="line_middle2";

    line_middle3=new THREE.Mesh(l_ver_geometry,l_material);
    line_middle3.renderOrder=100;
    line_middle3.position.set( 100, 0, 100);
    line_middle3.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    line_middle3.name="line_middle3";

    line_middle4=new THREE.Mesh(l_ver_geometry,l_material);
    line_middle4.renderOrder=100;
    line_middle4.position.set( -100, 0, -100 );
    line_middle4.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    line_middle4.name="line_middle4";

    line_bottom1=new THREE.Mesh(l_hol_geometry,l_material);
    line_bottom1.renderOrder=100;
    line_bottom1.position.set( -100, -50, 0 );
    line_bottom1.rotation.set(1/2*Math.PI,0,0);
    line_bottom1.name="line_bottom1";

    line_bottom2=new THREE.Mesh(l_hol_geometry,l_material);
    line_bottom2.renderOrder=100;
    line_bottom2.position.set( 100, -50, 0 );
    line_bottom2.rotation.set(1/2*Math.PI,0,0);
    line_bottom2.name="line_bottom2";

    line_bottom3=new THREE.Mesh(l_hol_geometry,l_material);
    line_bottom3.renderOrder=100;
    line_bottom3.position.set( 0, -50, 100);
    line_bottom3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    line_bottom3.name="line_bottom3";

    line_bottom4=new THREE.Mesh(l_hol_geometry,l_material);
    line_bottom4.renderOrder=100;
    line_bottom4.position.set( 0, -50, -100 );
    line_bottom4.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    line_bottom4.name="line_bottom4";

    meshList.push(plane1,plane2,plane3,plane4,plane5,plane6);

    meshList2.push(line_top1,line_top2,line_top3,line_top4,line_middle1,line_middle2,line_middle3,line_middle4,line_bottom1,line_bottom2,line_bottom3,line_bottom4);

    scene.add( text_A,text_B,text_C,text_D,text_E,text_F,text_G,text_H );

    scene.add(plane1,plane2,plane3,plane4,plane5,plane6,line_top1,line_top2,line_top3,line_top4,line_middle1,line_middle2,line_middle3,line_middle4,line_bottom1,line_bottom2,line_bottom3,line_bottom4);

    //gray lines inside box
    var bl_hol_geometry=new THREE.CylinderBufferGeometry( 1.2, 1.2, 198, 8,1,false, -0.15,1.78);
    var bl_hol_geometry2=new THREE.CylinderBufferGeometry(1.2, 1.2, 198, 8,1,false, -1.7,1.78);
    var bl_hol_geometry3=new THREE.CylinderBufferGeometry(1.2, 1.2, 99, 8,1,false, -1.7,1.85);
    var bl_hol_geometry4=new THREE.CylinderBufferGeometry( 1.2, 1.2, 99, 8,1,false, 1.4,1.85);
    var bl_hol_geometry5=new THREE.CylinderBufferGeometry( 1.2, 1.2, 99, 8,1,false, -3.3,1.85);
    var bl_hol_geometry6=new THREE.CylinderBufferGeometry( 1.2, 1.2, 99, 8,1,false, -0.2,1.85);
    var bl_hol_geometry7=new THREE.CylinderBufferGeometry( 1.2, 1.2, 198, 8,1,false, 1.5,1.78);
    var bl_hol_geometry8=new THREE.CylinderBufferGeometry(1.2, 1.2, 198, 8,1,false, 3.1,1.78);
    var bl_material=new THREE.MeshBasicMaterial({color: 0xffffff, side: THREE.FrontSide,transparent:true,opacity: 0.7});

    bline_top1=new THREE.Mesh(bl_hol_geometry,bl_material);
    bline_top1.renderOrder=10;
    bline_top1.position.set( -100, 50, 0 );
    bline_top1.rotation.set(1/2*Math.PI,0,0);
    bline_top1.name="bline_top1";

    bline_top2=new THREE.Mesh(bl_hol_geometry,bl_material);
    bline_top2.renderOrder=10;
    bline_top2.position.set( 0, 50, -100 );
    bline_top2.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    bline_top2.name="bline_top2";

    bline_top3=new THREE.Mesh(bl_hol_geometry2,bl_material);
    bline_top3.renderOrder=40;
    bline_top3.position.set( 0, 50, 100);
    bline_top3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    bline_top3.name="bline_top3";

    bline_top4=new THREE.Mesh(bl_hol_geometry2,bl_material);
    bline_top4.renderOrder=40;
    bline_top4.position.set( 100, 50, 0 );
    bline_top4.rotation.set(1/2*Math.PI,0,0);
    bline_top4.name="bline_top4";

    bline_middle1=new THREE.Mesh(bl_hol_geometry3,bl_material);
    bline_middle1.renderOrder=40;
    bline_middle1.position.set( -100, 0, 100 );
    bline_middle1.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    bline_middle1.name="bline_middle1";

    bline_middle2=new THREE.Mesh(bl_hol_geometry4,bl_material);
    bline_middle2.renderOrder=40;
    bline_middle2.position.set( 100, 0, -100 );
    bline_middle2.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    bline_middle2.name="bline_middle2";

    bline_middle3=new THREE.Mesh(bl_hol_geometry5,bl_material);
    bline_middle3.renderOrder=40;
    bline_middle3.position.set( 100, 0, 100);
    bline_middle3.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    bline_middle3.name="bline_middle3";

    bline_middle4=new THREE.Mesh(bl_hol_geometry6,bl_material);
    bline_middle4.renderOrder=40;
    bline_middle4.position.set( -100, 0, -100 );
    bline_middle4.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
    bline_middle4.name="bline_middle4";

    bline_bottom1=new THREE.Mesh(bl_hol_geometry7,bl_material);
    bline_bottom1.renderOrder=40;
    bline_bottom1.position.set( -100, -50, 0 );
    bline_bottom1.rotation.set(1/2*Math.PI,0,0);
    bline_bottom1.name="bline_bottom1";

    bline_bottom2=new THREE.Mesh(bl_hol_geometry7,bl_material);
    bline_bottom2.renderOrder=40;
    bline_bottom2.position.set( 0, -50, -100 );
    bline_bottom2.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    bline_bottom2.name="bline_bottom2";

    bline_bottom3=new THREE.Mesh(bl_hol_geometry8,bl_material);
    bline_bottom3.renderOrder=40;
    bline_bottom3.position.set( 0, -50, 100);
    bline_bottom3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
    bline_bottom3.name="bline_bottom3";

    bline_bottom4=new THREE.Mesh(bl_hol_geometry8,bl_material);
    bline_bottom4.renderOrder=40;
    bline_bottom4.position.set( 100, -50, 0 );
    bline_bottom4.rotation.set(1/2*Math.PI,0,0);
    bline_bottom4.name="bline_bottom4";

    scene.add(bline_top1,bline_top2,bline_top3,bline_top4,bline_middle1,bline_middle2,bline_middle3,bline_middle4,bline_bottom1,bline_bottom1,bline_bottom2,bline_bottom3,bline_bottom4);

    canvas = document.getElementById('three_view');
    canvas.addEventListener(mouseDown, startRotate);
    canvas.addEventListener(mouseMove, onRotate);
    canvas.addEventListener(mouseUp, endRotate);
    document.addEventListener(mouseUp, endRotate);

    //Add rotate box event
    $('#three_view').css('touch-action', 'none');
    $('*').on("contextmenu", function (e) {
        //Disable right click
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        return false;
    });

    $('*').on(mouseMove, function (e) {
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });
}

//button event
$("#button1").on("mousedown touchstart", function(){
    event.preventDefault();
    if(fun===0||fun===2){
      fun=1;
      select();
      $("#button1").addClass('active').css('background-image','url("img/select_1_active.png")');
      $("#button2").removeClass('active').css('background-image','url("img/select_2.png")');
    }else{
      fun=3;
      select();
      $("#button1").removeClass('active').css('background-image','url("img/select_1.png")');
    }
});

$("#button2").on("mousedown touchstart", function(){
    event.preventDefault();
    if(fun===0||fun===1){
        fun=2;
        select();
        $("#button2").addClass('active').css('background-image','url("img/select_2_active.png")');
        $("#button1").removeClass('active').css('background-image','url("img/select_1.png")')
        }else{
        fun=3;
        select();
        $("#button2").removeClass('active').css('background-image','url("img/select_2.png")')
    }
});

$("#button3").on("mousedown touchstart", function(){
    event.preventDefault();
    window.location.reload(true);
});

//button event changer
function select(){
    if(fun===1){
    canvas.removeEventListener(mouseDown, onDocumentMouseDown2);
    canvas.addEventListener(mouseDown, onDocumentMouseDown1);
    }else if(fun===2){
    canvas.removeEventListener(mouseDown, onDocumentMouseDown1);
    canvas.addEventListener(mouseDown, onDocumentMouseDown2);
    }else if(fun===3){
    canvas.removeEventListener(mouseDown, onDocumentMouseDown1);
    canvas.removeEventListener(mouseDown, onDocumentMouseDown2);
    fun=0;
    }else{return false}
};

function notRightClick(event) {
    if (event.nativeEvent) {
        return event.nativeEvent.which === 1 || !event.nativeEvent.which;
    } else {
        return event.which === 1 || !event.which || event.buttons === 1;
    }
}

function startRotate(e) {
    if (notRightClick(e)) {
        var x, y;
        switch (mouseMove) {
            case 'touchmove':
                x = e.targetTouches[0].clientX;
                y = e.targetTouches[0].clientY;
                break;
            case 'pointermove':
                if (e.isPrimary && pointerId === undefined) {
                    pointerId = e.pointerId;
                    x = e.pageX;
                    y = e.pageY;
                } else {
                    x = rotateStart.x;
                    y = rotateStart.y;
                }
                break;
            default:
                x = e.pageX;
                y = e.pageY;
                break;
        }
        rotateBox = true;
        rotateStart.x = x;
        rotateStart.y = y;
    }
}

function onRotate(e) {
    if (rotateBox && notRightClick(e)) {
        var x, y;
        switch (mouseMove) {
            case 'touchmove':
                x = e.targetTouches[0].clientX;
                y = e.targetTouches[0].clientY;
                break;
            case 'pointermove':
                if (e.pointerId === pointerId) {
                    x = e.pageX;
                    y = e.pageY;
                } else {
                    x = rotateStart.x;
                    y = rotateStart.y;
                }
                break;
            default:
                x = e.pageX;
                y = e.pageY;
                break;
        }
        scene.rotation.x += (y - rotateStart.y) / 100;
        var degX = Math.abs(scene.rotation.x % (2 * Math.PI));
        if (degX <= Math.PI / 2 || degX >= Math.PI * 1.5)
            scene.rotation.y += (x - rotateStart.x) / 100;
        else
            scene.rotation.y -= (x - rotateStart.x) / 100;
        rotateStart.x = x;
        rotateStart.y = y;
    }
}

function endRotate(e) {
    if (!notRightClick(e)) {
        return false;
    }
    if (pointerId === undefined) {
        rotateBox = false;
    } else if (e.pointerId === pointerId) {
        pointerId = undefined;
        rotateBox = false;
    }
}

function onDocumentMouseDown1( event ) {
    event.preventDefault();
    var element = event.currentTarget;
    // x and y coordinates on this canvas
    var wrap=document.querySelector('.project-wrap');
    var style = window.getComputedStyle(wrap);
    var margin=style.getPropertyValue('margin-left')
    var mar=parseInt(margin,10);
    var x0 = event.clientX ? event.clientX : (event.touches[0] || event.changedTouches[0] || {}).clientX;
    var y0 = event.clientY ? event.clientY : (event.touches[0] || event.changedTouches[0] || {}).clientY;

    var x = x0-(element.offsetLeft+mar);
    var y = y0-element.offsetTop;

    mouse.x = (x / width) * 2 - 1;
    mouse.y = - (y / height) * 2 + 1;

    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( meshList2 );
    if ( intersects.length > 0 ) {
      //rotation is disabled during mousedown
      rotateBox = false;

      selectObj = intersects[ 0 ].object;

      var l2_geometry=new THREE.CylinderBufferGeometry( 1, 1, 400, 100 );
      var l2_material=new THREE.MeshBasicMaterial({color: 0x7D98D4, side: THREE.FrontSide,transparent:true,opacity:1});
      const result = $.inArray(selectObj.name, list);

       if(result===-1&&selectObj.name==='line_top1'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_top1'){d=[];d.push(ind0[1])}
        });
       scene.children[d].visible=false;

       var line_t1=new THREE.Mesh(l2_geometry,l2_material);
       line_t1.position.set(-100,50,0);
       line_t1.rotation.set(1/2*Math.PI,0,0);
       line_t1.renderOrder=30;
       line_t1.name='line_t1';
       scene.add(line_t1);
       list.push(selectObj.name);
       }else if(list.indexOf('line_top1')>= 0&&selectObj.name==='line_top1'){
        var target='line_top1';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_t1'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_top1'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_top2'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_top4'){d=[];d.push(ind0[1])}
        });
       scene.children[d].visible=false;

       var line_t2=new THREE.Mesh(l2_geometry,l2_material);
       line_t2.position.set(100, 50, 0);
       line_t2.rotation.set(1/2*Math.PI,0,0);
       line_t2.renderOrder=30;
       line_t2.name='line_t2';
       scene.add(line_t2);
       list.push(selectObj.name);
       }else if(list.indexOf('line_top2')>= 0&&selectObj.name==='line_top2'){
        var target='line_top2';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_t2'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_top4'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_top3'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_top3'){d=[];d.push(ind0[1])}
        });
       scene.children[d].visible=false;

       var line_t3=new THREE.Mesh(l2_geometry,l2_material);
       line_t3.position.set(0, 50, 100);
       line_t3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
       line_t3.renderOrder=30;
       line_t3.name='line_t3';
       scene.add(line_t3);
       list.push(selectObj.name);
       }else if(list.indexOf('line_top3')>= 0&&selectObj.name==='line_top3'){
        var target='line_top3';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_t3'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_top3'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_top4'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_top2'){d=[];d.push(ind0[1])}
        });
       scene.children[d].visible=false;

       var line_t4=new THREE.Mesh(l2_geometry,l2_material);
       line_t4.position.set(0, 50, -100);
       line_t4.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
       line_t4.renderOrder=30;
       line_t4.name='line_t4';
       scene.add(line_t4);
       list.push(selectObj.name);
       }else if(list.indexOf('line_top4')>= 0&&selectObj.name==='line_top4'){
        var target='line_top4';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_t4'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_top2'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_middle1'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_middle1'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_m1=new THREE.Mesh(l2_geometry,l2_material);
       line_m1.position.set(-100, 0, 100);
       line_m1.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
       line_m1.renderOrder=30;
       line_m1.name='line_m1';
       scene.add(line_m1);
       list.push(selectObj.name);
       }else if(list.indexOf('line_middle1')>= 0&&selectObj.name==='line_middle1'){
        var target='line_middle1';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_m1'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_middle1'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

         if(result===-1&&selectObj.name==='line_middle2'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_middle2'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_m2=new THREE.Mesh(l2_geometry,l2_material);
       line_m2.position.set(100, 0, -100);
       line_m2.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
       line_m2.renderOrder=30;
       line_m2.name='line_m2';
       scene.add(line_m2);
       list.push(selectObj.name);
       }else if(list.indexOf('line_middle2')>= 0&&selectObj.name==='line_middle2'){
        var target='line_middle2';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_m2'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_middle2'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

         if(result===-1&&selectObj.name==='line_middle3'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_middle3'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_m3=new THREE.Mesh(l2_geometry,l2_material);
       line_m3.position.set(100, 0, 100);
       line_m3.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
       line_m3.renderOrder=30;
       line_m3.name='line_m3';
       scene.add(line_m3);
       list.push(selectObj.name);
       }else if(list.indexOf('line_middle3')>= 0&&selectObj.name==='line_middle3'){
        var target='line_middle3';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_m3'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_middle3'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

         if(result===-1&&selectObj.name==='line_middle4'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_middle4'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_m4=new THREE.Mesh(l2_geometry,l2_material);
       line_m4.position.set(-100, 0, -100);
       line_m4.rotation.set(1/2*Math.PI,1/2*Math.PI,1/2*Math.PI);
       line_m4.renderOrder=30;
       line_m4.name='line_m4';
       scene.add(line_m4);
       list.push(selectObj.name);
       }else if(list.indexOf('line_middle4')>= 0&&selectObj.name==='line_middle4'){
        var target='line_middle4';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_m4'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_middle4'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

         if(result===-1&&selectObj.name==='line_bottom1'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_bottom1'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;



       var line_b1=new THREE.Mesh(l2_geometry,l2_material);
       line_b1.position.set(-100, -50, 0);
       line_b1.rotation.set(1/2*Math.PI,0,0);
       line_b1.renderOrder=30;
       line_b1.name='line_b1';
       scene.add(line_b1);
       list.push(selectObj.name);
       }else if(list.indexOf('line_bottom1')>= 0&&selectObj.name==='line_bottom1'){
        var target='line_bottom1';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_b1'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_bottom1'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_bottom2'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_bottom4'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_b2=new THREE.Mesh(l2_geometry,l2_material);
       line_b2.position.set(100, -50, 0);
       line_b2.rotation.set(1/2*Math.PI,0,0);
       line_b2.renderOrder=30;
       line_b2.name='line_b2';
       scene.add(line_b2);
       list.push(selectObj.name);
       }else if(list.indexOf('line_bottom2')>= 0&&selectObj.name==='line_bottom2'){
        var target='line_bottom2';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_b2'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_bottom4'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

          if(result===-1&&selectObj.name==='line_bottom3'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_bottom3'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_b3=new THREE.Mesh(l2_geometry,l2_material);
       line_b3.position.set(0, -50, 100);
       line_b3.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
       line_b3.renderOrder=30;
       line_b3.name='line_b3';
       scene.add(line_b3);
       list.push(selectObj.name);
       }else if(list.indexOf('line_bottom3')>= 0&&selectObj.name==='line_bottom3'){
        var target='line_bottom3';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_b3'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_bottom3'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }

        if(result===-1&&selectObj.name==='line_bottom4'){
        scene.children.forEach(function (element){
          var ind0=[element.name,scene.children.indexOf(element)];
          if(ind0[0]==='bline_bottom2'){d=[];d.push(ind0[1])}
        });
        scene.children[d].visible=false;

       var line_b4=new THREE.Mesh(l2_geometry,l2_material);
       line_b4.position.set(0, -50, -100);
       line_b4.rotation.set(1/2*Math.PI,0,1/2*Math.PI);
       line_b4.name='line_b4';
       line_b4.renderOrder=30;
       scene.add(line_b4);
       list.push(selectObj.name);
       }else if(list.indexOf('line_bottom4')>= 0&&selectObj.name==='line_bottom4'){
        var target='line_bottom4';list.some(function(v, i){if (v==target) list.splice(i,1)});

        scene.children.forEach(function (element){
          var ind=[element.name,scene.children.indexOf(element)];
          if(ind[0]==='line_b4'){a=[];a.push(ind[1])}
          if(ind[0]==='bline_bottom2'){d=[];d.push(ind[1])}
        });
        scene.remove(scene.children[a]);
        scene.children[d].visible=true;
        }
    }
}

function onDocumentMouseDown2( event ) {
    event.preventDefault();
    var element = event.currentTarget;
    // x and y coordinates on this canvas
    var wrap=document.querySelector('.project-wrap');
    var style = window.getComputedStyle(wrap);
    var margin=style.getPropertyValue('margin-left')
    var mar=parseInt(margin,10);
    var x0 = event.clientX ? event.clientX : (event.touches[0] || event.changedTouches[0] || {}).clientX;
    var y0 = event.clientY ? event.clientY : (event.touches[0] || event.changedTouches[0] || {}).clientY;

    var x = x0-(element.offsetLeft+mar);
    var y = y0-element.offsetTop;

    mouse.x = (x / width) * 2 - 1;
    mouse.y = - (y / height) * 2 + 1;

    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( meshList );

  if ( intersects.length > 0 ) {
    //rotation is disabled during mousedown
    rotateBox = false;

    selectObj = intersects[ 0 ].object;

    var p2_geometry=new THREE.BoxGeometry( 250, 120,0.5);
    var p2_geometry2=new THREE.BoxGeometry( 250, 250,0.5);
    var p2_material=new THREE.MeshBasicMaterial({vertexColors: THREE.FaceColors, side: THREE.FrontSide,transparent:true,opacity: 0.5});

    for (var i = 0; i < p2_geometry.faces.length; i ++) {
    if(i<=9){
    p2_geometry.faces[ i ].color.setHex( 0xEAEEF2 );
    }else{
    p2_geometry.faces[ i ].color.setHex( 0xF4F4F4 )
    }
    }

    for (var i = 0; i < p2_geometry2.faces.length; i ++) {
    if(i<=9){
    p2_geometry2.faces[ i ].color.setHex( 0xEAEEF2 );
    }else{
    p2_geometry2.faces[ i ].color.setHex( 0xF4F4F4 )
    }
    }

    var p2_geometry3=new THREE.BoxGeometry( 250, 120,0.5);
    var p2_material3=new THREE.MeshBasicMaterial({vertexColors: THREE.FaceColors, side: THREE.FrontSide,transparent:true,opacity: 0.5});
    for (var i = 0; i < p2_geometry3.faces.length; i ++) {
    if(i<=7){
    p2_geometry3.faces[ i ].color.setHex( 0xEAEEF2 );
    }else if(i>=8&&i<=9){
    p2_geometry3.faces[ i ].color.setHex( 0xF4F4F4 );
    }else{
    p2_geometry3.faces[ i ].color.setHex( 0xEAEEF2 )
    }
    }

    var p2_geometry4=new THREE.BoxGeometry( 250, 250,0.5);
    var p2_material4=new THREE.MeshBasicMaterial({vertexColors: THREE.FaceColors, side: THREE.FrontSide,transparent:true,opacity: 0.5});
    for (var i = 0; i < p2_geometry4.faces.length; i ++) {
    if(i<=7){
    p2_geometry4.faces[ i ].color.setHex( 0xEAEEF2 );
    }else if(i>=8&&i<=9){
    p2_geometry4.faces[ i ].color.setHex( 0xF4F4F4 );
    }else{
    p2_geometry4.faces[ i ].color.setHex( 0xEAEEF2 )
    }
    }

    var p2_edge_geometry = new THREE.EdgesGeometry( p2_geometry );
    var p2_edge_geometry2 = new THREE.EdgesGeometry( p2_geometry2 );
    var p2_edge_material = new THREE.LineBasicMaterial({ color: 0xFFA500,side: THREE.FrontSide,transparent:true});

    const result = $.inArray(selectObj.name, list);

    if(num<2&&result===-1&&selectObj.name==='1'){
      num++;
     var plane1_1=new THREE.Mesh(p2_geometry,p2_material);
     var line1_1=new THREE.LineSegments(p2_edge_geometry,p2_edge_material);
     plane1_1.position.set(0,0,100.5);line1_1.position.set(0,0,100.5);
     plane1_1.name='plane1_1';line1_1.name='line1_1';
     plane1_1.renderOrder=50;line1_1.renderOrder=50;
     scene.add(plane1_1,line1_1);
     list.push(selectObj.name);
     if(num===2){return false}
    }else if(num>=0&&list.indexOf('1')>= 0&&selectObj.name==='1'){
      num--;
      var target='1';list.some(function(v, i){if (v==target) list.splice(i,1)});

      scene.children.forEach(function (element){
        var ind=[element.name,scene.children.indexOf(element)];
        if(ind[0]==='plane1_1'){a=[];a.push(ind[1])}
        if(ind[0]==='line1_1'){b=[];b.push(ind[1])}
      });
      scene.remove(scene.children[a],scene.children[b]);
    }

    if(num<2&&result===-1&&selectObj.name==='2'){
      num++;
     var plane2_1=new THREE.Mesh(p2_geometry,p2_material);
     var line2_1=new THREE.LineSegments(p2_edge_geometry,p2_edge_material);
     plane2_1.rotation.set(0,1/2*Math.PI,0);line2_1.rotation.set(0,1/2*Math.PI,0);
     plane2_1.position.set(100.5,0,0);line2_1.position.set(100.5,0,0);
     plane2_1.name='plane2_1';line2_1.name='line2_1';
     plane2_1.renderOrder=50;line2_1.renderOrder=50;
     scene.add(plane2_1,line2_1);
     list.push(selectObj.name);
     if(num===2){return false}
    }else if(num>=0&&list.indexOf('2')>= 0&&selectObj.name==='2'){
      num--;
      var target='2';list.some(function(v, i){if (v==target) list.splice(i,1)});

      scene.children.forEach(function (element){
        var ind=[element.name,scene.children.indexOf(element)];
        if(ind[0]==='plane2_1'){a=[];a.push(ind[1])}
        if(ind[0]==='line2_1'){b=[];b.push(ind[1])}
      });
      scene.remove(scene.children[a],scene.children[b]);
    }

    if(num<2&&result===-1&&selectObj.name==='3'){
     num++;
     var plane3_1=new THREE.Mesh(p2_geometry3,p2_material3);
     var line3_1=new THREE.LineSegments(p2_edge_geometry,p2_edge_material);
     plane3_1.rotation.set(0,1/2*Math.PI,0);line3_1.rotation.set(0,1/2*Math.PI,0);
     plane3_1.position.set(-100.5,0,0);line3_1.position.set(-100.5,0,0);
     plane3_1.name='plane3_1';line3_1.name='line3_1';
     plane3_1.renderOrder=50;line3_1.renderOrder=50;
     scene.add(plane3_1,line3_1);
     list.push(selectObj.name);
     if(num===2){return false}
    }else if(num>=0&&list.indexOf('3')>= 0&&selectObj.name==='3'){
      num--;
      var target='3';list.some(function(v, i){if (v==target) list.splice(i,1)});

      scene.children.forEach(function (element){
        var ind=[element.name,scene.children.indexOf(element)];
        if(ind[0]==='plane3_1'){a=[];a.push(ind[1])}
        if(ind[0]==='line3_1'){b=[];b.push(ind[1])}
      });
      scene.remove(scene.children[a],scene.children[b]);
    }

    if(num<2&&result===-1&&selectObj.name==='4'){
      num++;
     var plane4_1=new THREE.Mesh(p2_geometry3,p2_material3);
     var line4_1=new THREE.LineSegments(p2_edge_geometry,p2_edge_material);
     plane4_1.position.set(0,0,-100.5);line4_1.position.set(0,0,-100.5);
     plane4_1.name='plane4_1';line4_1.name='line4_1';
     plane4_1.renderOrder=50;line4_1.renderOrder=50;
     scene.add(plane4_1,line4_1);
     list.push(selectObj.name);
     if(num===2){return false;}
    }else if(num>=0&&list.indexOf('4')>= 0&&selectObj.name==='4'){
      num--;
      var target='4';list.some(function(v, i){if (v==target) list.splice(i,1)});

      scene.children.forEach(function (element){
        var ind=[element.name,scene.children.indexOf(element)];
        if(ind[0]==='plane4_1'){a=[];a.push(ind[1])}
        if(ind[0]==='line4_1'){b=[];b.push(ind[1])}
      });
      scene.remove(scene.children[a],scene.children[b]);
    }

    if(num<2&&result===-1&&selectObj.name==='5'){
      num++;
     var plane5_1=new THREE.Mesh(p2_geometry2,p2_material);
     var line5_1=new THREE.LineSegments(p2_edge_geometry2,p2_edge_material);
     plane5_1.rotation.set(1/2*Math.PI,0,0);line5_1.rotation.set(1/2*Math.PI,0,0);
     plane5_1.position.set(0,-50.5,0);line5_1.position.set(0,-50.5,0);
     plane5_1.name='plane5_1';line5_1.name='line5_1';
     plane5_1.renderOrder=50;line5_1.renderOrder=50;
     scene.add(plane5_1,line5_1);
     list.push(selectObj.name);
     if(num===2){return false;}
    }else if(num>=0&&list.indexOf('5')>= 0&&selectObj.name==='5'){
      num--;
      var target='5';list.some(function(v, i){if (v==target) list.splice(i,1)});

      scene.children.forEach(function (element){
        var ind=[element.name,scene.children.indexOf(element)];
        if(ind[0]==='plane5_1'){a=[];a.push(ind[1])}
        if(ind[0]==='line5_1'){b=[];b.push(ind[1])}
      });
      scene.remove(scene.children[a],scene.children[b]);
    }

    if(num<2&&result===-1&&selectObj.name==='6'){
       num++;
       var plane6_1=new THREE.Mesh(p2_geometry4,p2_material4);
       var line6_1=new THREE.LineSegments(p2_edge_geometry2,p2_edge_material);
       plane6_1.rotation.set(1/2*Math.PI,0,0);line6_1.rotation.set(1/2*Math.PI,0,0);
       plane6_1.position.set(0,50.5,0);line6_1.position.set(0,50.5,0);
       plane6_1.name='plane6_1';line6_1.name='line6_1';
       plane6_1.renderOrder=50;line6_1.renderOrder=50;
       scene.add(plane6_1,line6_1);
       list.push(selectObj.name);
       if(num===2){return false;}
    }else if(num>=0&&list.indexOf('6')>= 0&&selectObj.name==='6'){
    num--;
    var target='6';list.some(function(v, i){if (v==target) list.splice(i,1)});

    scene.children.forEach(function (element){
      var ind=[element.name,scene.children.indexOf(element)];
      if(ind[0]==='plane6_1'){a=[];a.push(ind[1])}
      if(ind[0]==='line6_1'){b=[];b.push(ind[1])}
    });
    scene.remove(scene.children[a],scene.children[b]);
   }
  }
}

function render() {
    requestAnimationFrame(render);
    renderer.render(scene, camera);
    labelRenderer.render(scene, camera);
}

// //Check touch device
function isTouchDevice() {
    return 'ontouchstart' in window        // check on most browsers
        || navigator.maxTouchPoints;       // check on IE10/11 and Surface
}

//Check Browser
function getBrowserName() {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    var name = "Unknown";
    if (isEdge) {
        name = "Edge";
    } else if (isIE) {
        name = "IE"
    } else if (isFirefox) {
        name = "Firefox";
    } else if (isOpera) {
        name = "Opera";
    } else if (isChrome) {
        name = "Chrome";
    } else if (isSafari) {
        name = "Safari";
    }
    return name;
}

document.documentElement.addEventListener('touchstart', function (e) {
  if (e.touches.length >= 2) {e.preventDefault();}
  }, {passive: false});
  /* no dbltap zoom for safari*/
var t = 0;
document.documentElement.addEventListener('touchend', function (e) {
  var now = new Date().getTime();
  if ((now - t) < 350){
    e.preventDefault();
  }
  t = now;
}, false);
