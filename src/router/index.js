import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'
import a109 from '../views/a1-09.vue'
import s110 from '../views/s1-10.vue'
import s114 from '../views/s1-14.vue'
import s116 from '../views/s1-16.vue'
import s117 from '../views/s1-17.vue'
import s118 from '../views/s1-18.vue'
import s119 from '../views/s1-19.vue'
import s120 from '../views/s1-20.vue'
import s121 from '../views/s1-21.vue'
import s122 from '../views/s1-22.vue'
import s124 from '../views/s1-24.vue'
import s125 from '../views/s1-25.vue'
import s126 from '../views/s1-26.vue'
import s127 from '../views/s1-27.vue'
import s128 from '../views/s1-28.vue'
import s129 from '../views/s1-29.vue'
import s215 from '../views/s2-15.vue'
import s224 from '../views/s2-24.vue'
import s226 from '../views/s2-26.vue'
import s322 from '../views/s3-22.vue'
import s323 from '../views/s3-23.vue'
import s324 from '../views/s3-24.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/a1-09',
    name: 'a1-09',
    component: a109
  },
  {
    path: '/s1-10',
    name: 's1-10',
    component: s110
  },
  {
    path: '/s1-14',
    name: 's1-14',
    component: s114
  },
  {
    path: '/s1-16',
    name: 's1-16',
    component: s116
  },
  {
    path: '/',
    name: 's1-17',
    component: s117
  },

  {
    path: '/s1-18',
    name: 's1-18',
    component: s118
  },
  {
    path: '/s1-19',
    name: 's1-19',
    component: s119
  },
  {
    path: '/s1-20',
    name: 's1-20',
    component: s120
  },
  {
    path: '/s1-21',
    name: 's1-21',
    component: s121
  },
  {
    path: '/s1-22',
    name: 's1-22',
    component: s122
  },
  {
    path: '/s1-24',
    name: 's1-24',
    component: s124
  },
  {
    path: '/s1-25',
    name: 's1-25',
    component: s125
  },
  {
    path: '/s1-26',
    name: 's1-26',
    component: s126
  },
  {
    path: '/s1-27',
    name: 's1-27',
    component: s127
  },
  {
    path: '/s1-28',
    name: 's1-28',
    component: s128
  },
  {
    path: '/s1-29',
    name: 's1-29',
    component: s129
  },
  {
    path: '/s2-24',
    name: 's2-24',
    component: s224
  },
  {
    path: '/s2-15',
    name: 's2-15',
    component: s215
  },
  {
    path: '/s2-26',
    name: 's2-26',
    component: s226
  },
  {
    path: '/s3-22',
    name: 's3-22',
    component: s322
  },
  {
    path: '/s3-23',
    name: 's3-23',
    component: s323
  },
  {
    path: '/s3-24',
    name: 's3-24',
    component: s324
  },
]

const router = new VueRouter({
  routes
})

export default router
